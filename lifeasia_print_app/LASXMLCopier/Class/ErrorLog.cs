﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LASXMLCopier.Class
{
    public static class ErrorLog
    {
        private static string ErrorLogFolder = AppDomain.CurrentDomain.BaseDirectory + "ErrorLog";
        public static void Save(string filename, string text)
        {
            if (!Directory.Exists(ErrorLogFolder))
                Directory.CreateDirectory(ErrorLogFolder);

            string filePath = ErrorLogFolder + "\\" + filename + ".txt";

            if (File.Exists(filePath))
                File.Delete(filePath);

            TextWriter tw = new StreamWriter(filePath);
            tw.WriteLine(text);
            tw.Close();
        }
    }
}
