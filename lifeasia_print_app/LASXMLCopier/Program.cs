﻿using LASXMLCopier.Class;
using LASXMLCopier.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace LASXMLCopier
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            while (true)
            {
                DateTime today = DateTime.Today;
                string lasPath = Properties.Settings.Default.LASFILEPATH;
                string outputPath = Properties.Settings.Default.XMLSTORAGE;
                string appPath = AppDomain.CurrentDomain.BaseDirectory + @"Temp\\";
                string temp = null;
                PRD_LPS_DB db = new PRD_LPS_DB();
                List<FileInfo> Files = new List<FileInfo>();
                List<LETTER> letterList = db.LETTERs.ToList();
                try
                { 
                    lasPath += "\\" + today.Year.ToString() + today.ToString("MMM").ToUpper();

                    //Prepare Folder
                    if (!Directory.Exists(outputPath))
                        Directory.CreateDirectory(outputPath);
                    if (!Directory.Exists(appPath))
                        Directory.CreateDirectory(appPath);
                    //END Prepare Folder

                    DirectoryInfo dir = new DirectoryInfo(lasPath);
                    Files = null;
                    Files = dir.GetFiles("*.XML").OrderByDescending(x => x.CreationTime).ToList();
                }
                catch (Exception ex)
                {
                    ErrorLog.Save("IFS Connection Error_"+DateTime.Now.ToString("yyyyMMdd hhmm"), ex.ToString());
                    continue;
                }
                foreach (FileInfo file in Files)
                {
                    if (db.LETTER_CPY_LOG.Where(row => row.OriginalFile == file.Name).Count() > 0)
                        break;
                    try
                    {
                        //Copy to Application Folder (Local)
                        temp = appPath + file.Name;
                        File.Copy(file.FullName, temp, true);

                        LetterReader letter = new LetterReader(temp);
                        string newfilename = letter.ContrNo + letter.LETTERCTL + letter.DOCIDNUM + ".XML";

                        if (letterList.Where(row => row.LetterType == letter.LETTERCTL).Count() == 0)
                        {
                            string foldertosave = outputPath + "\\Not Categorized\\" + today.ToString("MMM") + "\\" + today.Day.ToString() + "\\";
                            if (!Directory.Exists(foldertosave))
                                Directory.CreateDirectory(foldertosave);
                            foldertosave += "\\" + today.ToString("MMM");
                            if (!Directory.Exists(foldertosave))
                                Directory.CreateDirectory(foldertosave);
                            foldertosave += "\\" + today.Day.ToString();
                            if (!Directory.Exists(foldertosave))
                                Directory.CreateDirectory(foldertosave);
                            File.Copy(temp, foldertosave + newfilename, true);
                        }
                        else
                        {
                            string foldertosave = outputPath + "\\" + letterList.Where(row => row.LetterType == letter.LETTERCTL).FirstOrDefault().LetterFolder;
                            if (!Directory.Exists(foldertosave))
                                Directory.CreateDirectory(foldertosave);
                            foldertosave += "\\" + today.ToString("MMM");
                            if (!Directory.Exists(foldertosave))
                                Directory.CreateDirectory(foldertosave);
                            foldertosave += "\\" + today.Day.ToString();
                            if (!Directory.Exists(foldertosave))
                                Directory.CreateDirectory(foldertosave);
                            File.Copy(temp, foldertosave + "\\" + newfilename, true);
                        }

                        //Save Log 
                        LETTER_CPY_LOG log = new LETTER_CPY_LOG
                        {
                            Policy = letter.ContrNo,
                            Type = letter.LETTERCTL,
                            Version = letter.DOCIDNUM,
                            OriginalFile = file.Name,
                            OutputFile = newfilename,
                            DateTime = DateTime.Now
                        };
                        db.LETTER_CPY_LOG.Add(log);
                        db.SaveChanges();

                        if (System.IO.File.Exists(temp))
                            System.IO.File.Delete(temp);
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.Save("File Copy Process Error_" + DateTime.Now.ToString("yyyyMMdd hhmm"), ex.ToString());
                        continue;
                    }
                }
                Thread.Sleep(1000); //select 1 second
            }
        } 
    }
}
